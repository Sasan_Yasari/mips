module sev_seg(
	input clk, 
	input[3:0] count, 
	output reg[6:0] sev
	);
	
	always@(posedge clk) begin
		case(count)
			0: sev = 7'b1000000;
			1: sev = 7'b1001111;
			2: sev = 7'b0100100;
			3: sev = 7'b0110000;
			4: sev = 7'b0011001;
			5: sev = 7'b0010010;
			6: sev = 7'b0000010;
			7: sev = 7'b1111000;
			8: sev = 7'b0000000;
			9: sev = 7'b0010000;
			10: sev = 7'b0001000;
			11: sev = 7'b0000011;
			12: sev = 7'b1000110;
			default:  sev = 7'b0000110;
		endcase
	end
endmodule
