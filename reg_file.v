module reg_file(
	input clk,
	input rst, 
	input[4:0] src1, 
	input[4:0] src2, 
	input[4:0] dest, 
	input[31:0] Write_Val, 
	input Write_EN, 
	output[31:0] reg1, 
	output[31:0] reg2
	);
	
	reg[31:0] mem [0:31];
	integer i;
	always@(posedge clk, posedge rst) begin
	  if(rst == 1)
		for(i = 0; i < 32; i = i + 1)
			mem[i] = i;
    else if(Write_EN == 1 && dest != 0)
      mem[dest] = Write_Val;
  end
	assign reg1 = mem[src1];
	assign reg2 = mem[src2];
  
endmodule
