module hazard_detection_unit(
	input rst, 
	input[4:0] src1, 
	input[4:0] src2, 
	input[4:0] Exe_Dest, 
	input[4:0] Mem_Dest, 
	input Exe_mem_R_EN, 
	input Mem_mem_R_EN, 
	input MEM_WB_EN, 
	input EXE_WB_EN, 
	input[1:0] branch, 
	output reg hazard_Detected
	);
	
	always@(*) begin
		if(rst) 
			hazard_Detected = 0;
		else begin
			if(Exe_mem_R_EN && (Exe_Dest == src1 || Exe_Dest == src2) && Exe_Dest != 0 && EXE_WB_EN)
				hazard_Detected = 1;
			else if((branch == 2'b00 || branch == 2'b01) && MEM_WB_EN)
				hazard_Detected = 1;
			else if((branch == 2'b00 || branch == 2'b01) && EXE_WB_EN)
				hazard_Detected = 1;
			else
				hazard_Detected = 0;
		end
	end
endmodule
