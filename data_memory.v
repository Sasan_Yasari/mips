module data_memory(
	input clk, 
	input rst, 
	input MEM_W_EN, 
	input MEM_R_EN, 
	input[31:0] ALU_result, 
	input[31:0] ST_value, 
	output[31:0] data
	);
	
	reg [31:0] memory [0:63];
	integer i;
	always@(posedge clk, posedge rst) begin
		if(rst == 1)
			for(i = 0; i < 64; i = i + 1)
				memory[i] = 0;
		else if(MEM_W_EN)
			memory[(ALU_result>>2)-256] = ST_value;
	end
	assign data = MEM_R_EN ? memory[(ALU_result>>2)-256] : 32'b0;
endmodule
