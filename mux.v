module mux(
	input sel, 
	input[31:0] in1_mux, 
	input[31:0] in2_mux, 
	output[31:0] out_mux
	);
	
	assign out_mux = (sel == 0) ? in1_mux : in2_mux;

endmodule
