module condition_check(
	input rst, 
	input[31:0] reg1, 
	input[31:0] reg2, 
	input[1:0] Branch_Type, 
	output reg Is_Branch
	);
	
	always@(*) begin
		if(rst)
			Is_Branch = 0;
		else begin
			case(Branch_Type)
				2'b00: Is_Branch = (reg1 == 32'd0) ? 1'b1 : 1'b0;
				2'b01: Is_Branch = (reg1 == reg2) ? 1'b0 : 1'b1;
				2'b10: Is_Branch =  1'b1;
				default: Is_Branch = 1'b0;
			endcase
		end
	end
endmodule
