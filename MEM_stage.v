module MEM_stage(
	input clk, 
	input rst, 
	input[31:0] address, 
	input[31:0] write_data, 
	input r_en, 
	input we_en, 
	output [31:0] read_data, 
	output ready, 
	inout [15:0] DATA, 
	output [17:0] sram_address_out, 
	output SRAM_UB_N, 
	output SRAM_LB_N, 
	output SRAM_WE_N, 
	output SRAM_CE_N, 
	output SRAM_OE_N
	);

	wire[31:0] sram_address;
	wire[31:0] sram_wdata;
	wire sram_read, sram_write, sram_ready;
	wire [63:0] sram_rdata;
	cache_controller(clk, rst, address, write_data, r_en, we_en, read_data, ready, sram_address, sram_wdata, sram_read, sram_write, sram_rdata, sram_ready
	);
	
	assign DATA = we_en ? sram_write : r_en ? sram_read : 16'bz; 
	
	SRAM_Controller sc(clk, rst, sram_address, sram_wdata, sram_read, sram_write, sram_rdata, sram_ready, DATA, sram_address_out_, SRAM_UB_N_O, SRAM_LB_N_O, SRAM_WE_N_O, SRAM_CE_N_O, SRAM_OE_N_O);
	
	
	assign sram_address_out = sram_address_out_;
	assign SRAM_UB_N = SRAM_UB_N_O;
	assign SRAM_LB_N = SRAM_LB_N_O;
	assign SRAM_WE_N = SRAM_WE_N_O;
	assign SRAM_CE_N = SRAM_CE_N_O;
	assign SRAM_OE_N = SRAM_OE_N_O;
	

	
endmodule
