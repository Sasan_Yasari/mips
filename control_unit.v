module control_unit(
	input rst, 
	input[5:0] op_code, 
	output reg ST_or_BNE, 
	output reg is_imm, 
	output reg cond_and, 
	output reg[1:0] Branch_Type, 
	output reg[3:0] EXE_CMD, 
	output reg MEM_R_EN, 
	output reg MEM_W_EN, 
	output reg WB_EN
	);
	
	always@(*) begin
		if(rst) begin
			ST_or_BNE = 0;
			is_imm = 0;
			cond_and = 0;
			Branch_Type = 0;
			EXE_CMD = 0;
			MEM_R_EN = 0;
			MEM_W_EN = 0;
			WB_EN = 0;
		end
		else begin
			case(op_code)
			  6'd0: begin EXE_CMD = 4'bxxxx; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b0; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd1: begin EXE_CMD = 4'b0000; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd3: begin EXE_CMD = 4'b0010; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd5: begin EXE_CMD = 4'b0100; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd6: begin EXE_CMD = 4'b0101; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd7: begin EXE_CMD = 4'b0110; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd8: begin EXE_CMD = 4'b0111; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd9: begin EXE_CMD = 4'b1100; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd10: begin EXE_CMD = 4'b1000; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd11: begin EXE_CMD = 4'b1001; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd12: begin EXE_CMD = 4'b1010; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; is_imm = 0; Branch_Type = 2'b11; cond_and = 0; end
			  6'd32: begin EXE_CMD = 4'b0000; is_imm = 1; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; Branch_Type = 2'b11; cond_and = 0; end
			  6'd33: begin EXE_CMD = 4'b0010; is_imm = 1; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b1; Branch_Type = 2'b11; cond_and = 0; end
			  6'd36: begin EXE_CMD = 4'b0000; ST_or_BNE = 1'b0; MEM_W_EN = 1'b0; MEM_R_EN = 1'b1; WB_EN=1'b1; is_imm = 1; Branch_Type = 2'b11; cond_and = 0; end
			  6'd37: begin EXE_CMD = 4'b0000; ST_or_BNE = 1'b1; MEM_W_EN = 1'b1; MEM_R_EN = 1'b0; WB_EN=1'b0; is_imm = 1; Branch_Type = 2'b11; cond_and = 0; end
			  6'd40: begin EXE_CMD = 4'bXXXX; cond_and = 1; Branch_Type = 2'b00; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b0; is_imm = 1; end 
			  6'd41: begin EXE_CMD = 4'bXXXX; cond_and = 1; Branch_Type = 2'b01; ST_or_BNE = 1'b1; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b0; is_imm = 1; end
			  6'd42: begin EXE_CMD = 4'bXXXX; cond_and = 1; Branch_Type = 2'b10; ST_or_BNE = 1'b0; MEM_R_EN = 1'b0; MEM_W_EN = 1'b0; WB_EN=1'b0; is_imm = 1; end
			  default: begin EXE_CMD = 4'bXXXX; Branch_Type=2'bXX; is_imm = 0; cond_and = 0; ST_or_BNE = 1'b0; MEM_R_EN = 1'bx; MEM_W_EN = 1'bx; WB_EN=1'bx; is_imm = 1'bx; end
			endcase
		end
	end
endmodule
