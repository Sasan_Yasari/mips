module ID_stage_reg(
	input clk,
	input rst, 
	input freeze, 
	input[4:0] Dest_in, 
	input[4:0] src1, 
	input[4:0] src2, 
	input[31:0] Reg2_in, 
	input[31:0] Val2_in, 
	input[31:0] Val1_in, 
	input[31:0] PC_in, 
	input Br_taken_in, 
	input[3:0] EXE_CMD_in, 
	input MEM_R_EN_in, 
	input MEM_W_EN_in, 
	input WB_EN_in, 
	output reg[4:0] Dest, 
	output reg[4:0] src1_out, 
	output reg[4:0] src2_out, 
	output reg[31:0] Reg2, 
	output reg[31:0] Val2, 
	output reg[31:0] Val1, 
	output reg[31:0] PC_out, 
	output reg Br_taken, 
	output reg[3:0] EXE_CMD, 
	output reg MEM_R_EN, 
	output reg MEM_W_EN, 
	output reg WB_EN
	);
	
  
	always@(negedge clk, posedge rst) begin
		if(rst == 1) begin 
			Dest = 0;
			Reg2 = 0;
			Val2 = 0;
			Val1 = 0;
			PC_out = 0;
			Br_taken = 0;
			EXE_CMD = 0;
			MEM_R_EN = 0;
			MEM_W_EN = 0;
			WB_EN = 0;
			src1_out = 0;
			src2_out = 0;
		end 
		else if(freeze) begin
			Dest = Dest;
			Reg2 = Reg2;
			Val2 = Val2;
			Val1 = Val1;
			PC_out = PC_out;
			Br_taken = Br_taken;
			EXE_CMD = EXE_CMD;
			MEM_R_EN = MEM_R_EN;
			MEM_W_EN = MEM_W_EN;
			WB_EN = WB_EN;
			src1_out = src1_out;
			src2_out = src2_out;
		end
		else begin
			Dest = Dest_in;
			Reg2 = Reg2_in;
			Val2 = Val2_in;
			Val1 = Val1_in;
			PC_out = PC_in;
			Br_taken = Br_taken_in;
			EXE_CMD = EXE_CMD_in;
			MEM_R_EN = MEM_R_EN_in;
			MEM_W_EN = MEM_W_EN_in;
			WB_EN = WB_EN_in;
			src1_out = src1;
			src2_out = src2;
		end
	end                  
  
endmodule