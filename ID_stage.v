module ID_stage(
	input clk,
	input rst, 
	input[31:0] Instruction, 
	input[31:0] reg1, 
	input[31:0] reg2, 
	output[4:0] src1, 
	output[4:0] src2, 
	output[4:0] src1_f, 
	output[4:0] src2_f, 
	output IF_flush, 
	output[4:0] Dest, 
	output[31:0] Reg2, 
	output[31:0] Val2, 
	output[31:0] Val1, 
	output Br_taken, 
	output[3:0] EXE_CMD, 
	output MEM_R_EN, 
	output MEM_W_EN, 
	output WB_EN, 
	output is_imm, 
	output st_or_bne, 
	output[1:0] br
	);
               
  wire ST_or_BNE, is_Imm, cond_and, Is_Branch;
  wire[1:0] Branch_Type;
  wire[4:0] out_mux, src2_;
  wire[31:0] out_mux2, out_sign;
  wire cnt_MEM_R_EN, cnt_MEM_W_EN, cnt_WB_EN;
  wire[3:0] cnt_EXE_CMD;
  mux2 mux1(ST_or_BNE, Instruction[15:11], Instruction[25:21], out_mux);
  mux mux2(is_Imm, Reg2, out_sign, out_mux2);
  sign_ext se(rst, Instruction[15:0], out_sign);
  control_unit cu(rst, Instruction[31:26], ST_or_BNE, is_Imm, cond_and, Branch_Type, cnt_EXE_CMD, cnt_MEM_R_EN, cnt_MEM_W_EN, cnt_WB_EN);
  condition_check cc(rst, reg1, reg2, Branch_Type, Is_Branch);
  
  assign br = Branch_Type;
  assign Br_taken = cond_and && Is_Branch;
  
  assign is_imm = is_Imm; 
  assign st_or_bne = ST_or_BNE;
  
  assign Val2 = out_mux2;     
  assign src1 = Instruction[20:16];
  assign src2 = out_mux;
  //assign IF_flush = 
  assign Dest = Instruction[25:21]; 
  assign Reg2 = reg2;
  assign Val1 = reg1;
  assign EXE_CMD = cnt_EXE_CMD;
  assign MEM_R_EN = cnt_MEM_R_EN;
  assign MEM_W_EN = cnt_MEM_W_EN;
  assign WB_EN = cnt_WB_EN;
  
  mux2 m2(is_Imm, Instruction[15:11], 5'b0, src2_);
  assign src1_f = Instruction[20:16];
  assign src2_f = src2_;
endmodule 
