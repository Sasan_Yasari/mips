module EXE_stage(
	input clk, 
	input rst, 
	input[1:0] s1, 
	input[1:0] s2, 
	input[1:0] s3, 
	input[3:0] EXE_CMD, 
	input[31:0] val1, 
	input[31:0] val2, 
	input[31:0] reg2, 
	input[31:0] alu_result_mem, 
	input[31:0] result_wb, 
	output[31:0] ALU_result, 
	output[31:0] st_val
	);
	
	wire[31:0] out_mux1, out_mux2, out_mux3;
	ALU al(rst, out_mux1, out_mux2, EXE_CMD, ALU_result);
	mux3 m1(s1, val1, alu_result_mem, result_wb, out_mux1);
	mux3 m2(s2, val2, alu_result_mem, result_wb, out_mux2);
	mux3 m3(s3, reg2, alu_result_mem, result_wb, out_mux3);
	assign st_val = out_mux3;
endmodule
