module adder(
	input[31:0] in_adder1, 
	input[31:0] in_adder2, 
	output [31:0] out_adder
	);
	
	assign out_adder = in_adder1 + in_adder2; 

endmodule
