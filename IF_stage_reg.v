module IF_stage_reg(
	input clk, 
	input rst, 
	input flush, 
	input freeze, 
	input[31:0] PC_in, 
	input[31:0] Instruction_in, 
	output reg[31:0] PC, 
	output reg[31:0] Instruction
	);
	
	always@(negedge clk) begin
		if(rst || flush) begin
			PC = 0;  
			Instruction = 0;
		end
		else if(freeze)
			Instruction = Instruction;
		else begin
			PC = PC_in;  
			Instruction = Instruction_in;
		end
	end
	
endmodule
