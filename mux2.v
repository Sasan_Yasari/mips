module mux2(
	input sel, 
	input[4:0] in1_mux, 
	input[4:0] in2_mux, 
	output[4:0] out_mux
	);
	
	assign out_mux = (sel == 0) ? in1_mux : in2_mux;

endmodule
