library verilog;
use verilog.vl_types.all;
entity sign_ext is
    port(
        rst             : in     vl_logic;
        in_sign         : in     vl_logic_vector(15 downto 0);
        out_sign        : out    vl_logic_vector(31 downto 0)
    );
end sign_ext;
