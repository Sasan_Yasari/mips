library verilog;
use verilog.vl_types.all;
entity control_unit is
    port(
        rst             : in     vl_logic;
        op_code         : in     vl_logic_vector(5 downto 0);
        ST_or_BNE       : out    vl_logic;
        is_imm          : out    vl_logic;
        cond_and        : out    vl_logic;
        Branch_Type     : out    vl_logic_vector(1 downto 0);
        EXE_CMD         : out    vl_logic_vector(3 downto 0);
        MEM_R_EN        : out    vl_logic;
        MEM_W_EN        : out    vl_logic;
        WB_EN           : out    vl_logic
    );
end control_unit;
