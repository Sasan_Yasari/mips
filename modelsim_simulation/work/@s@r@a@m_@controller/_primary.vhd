library verilog;
use verilog.vl_types.all;
entity SRAM_Controller is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        \SRAM_address\  : in     vl_logic_vector(17 downto 0);
        SRAM_write_data : in     vl_logic_vector(31 downto 0);
        Sram_r_en       : in     vl_logic;
        Sram_we_en      : in     vl_logic;
        SRAM_read_data  : out    vl_logic_vector(31 downto 0);
        ready           : out    vl_logic;
        SRAM_DATA       : out    vl_logic_vector(15 downto 0);
        \SRAM_ADDRESS\  : out    vl_logic_vector(17 downto 0);
        SRAM_UB_N_O     : out    vl_logic;
        SRAM_LB_N_O     : out    vl_logic;
        SRAM_WE_N_O     : out    vl_logic;
        SRAM_CE_N_O     : out    vl_logic;
        SRAM_OE_N_O     : out    vl_logic
    );
end SRAM_Controller;
