library verilog;
use verilog.vl_types.all;
entity mux is
    port(
        sel             : in     vl_logic;
        in1_mux         : in     vl_logic_vector(31 downto 0);
        in2_mux         : in     vl_logic_vector(31 downto 0);
        out_mux         : out    vl_logic_vector(31 downto 0)
    );
end mux;
