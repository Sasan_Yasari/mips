library verilog;
use verilog.vl_types.all;
entity sev_seg is
    port(
        clk             : in     vl_logic;
        count           : in     vl_logic_vector(3 downto 0);
        sev             : out    vl_logic_vector(6 downto 0)
    );
end sev_seg;
