library verilog;
use verilog.vl_types.all;
entity PC is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        freeze          : in     vl_logic;
        in_PC           : in     vl_logic_vector(31 downto 0);
        out_PC          : out    vl_logic_vector(31 downto 0)
    );
end PC;
