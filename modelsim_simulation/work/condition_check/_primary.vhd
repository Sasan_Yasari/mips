library verilog;
use verilog.vl_types.all;
entity condition_check is
    port(
        rst             : in     vl_logic;
        reg1            : in     vl_logic_vector(31 downto 0);
        reg2            : in     vl_logic_vector(31 downto 0);
        Branch_Type     : in     vl_logic_vector(1 downto 0);
        Is_Branch       : out    vl_logic
    );
end condition_check;
