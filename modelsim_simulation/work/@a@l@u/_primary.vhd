library verilog;
use verilog.vl_types.all;
entity ALU is
    port(
        rst             : in     vl_logic;
        in1             : in     vl_logic_vector(31 downto 0);
        in2             : in     vl_logic_vector(31 downto 0);
        cmd             : in     vl_logic_vector(3 downto 0);
        result          : out    vl_logic_vector(31 downto 0)
    );
end ALU;
