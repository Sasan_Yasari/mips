library verilog;
use verilog.vl_types.all;
entity adder is
    port(
        in_adder1       : in     vl_logic_vector(31 downto 0);
        in_adder2       : in     vl_logic_vector(31 downto 0);
        out_adder       : out    vl_logic_vector(31 downto 0)
    );
end adder;
