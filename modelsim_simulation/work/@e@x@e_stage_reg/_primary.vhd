library verilog;
use verilog.vl_types.all;
entity EXE_stage_reg is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        freeze          : in     vl_logic;
        WB_en_in        : in     vl_logic;
        MEM_R_EN_in     : in     vl_logic;
        MEM_W_EN_in     : in     vl_logic;
        PC_in           : in     vl_logic_vector(31 downto 0);
        ALU_result_in   : in     vl_logic_vector(31 downto 0);
        ST_val_in       : in     vl_logic_vector(31 downto 0);
        Dest_in         : in     vl_logic_vector(4 downto 0);
        WB_en           : out    vl_logic;
        MEM_R_EN        : out    vl_logic;
        MEM_W_EN        : out    vl_logic;
        PC              : out    vl_logic_vector(31 downto 0);
        ALU             : out    vl_logic_vector(31 downto 0);
        ST_val          : out    vl_logic_vector(31 downto 0);
        Dest            : out    vl_logic_vector(4 downto 0)
    );
end EXE_stage_reg;
