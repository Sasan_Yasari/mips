library verilog;
use verilog.vl_types.all;
entity hazard_detection_unit is
    port(
        rst             : in     vl_logic;
        src1            : in     vl_logic_vector(4 downto 0);
        src2            : in     vl_logic_vector(4 downto 0);
        Exe_Dest        : in     vl_logic_vector(4 downto 0);
        Mem_Dest        : in     vl_logic_vector(4 downto 0);
        Exe_mem_R_EN    : in     vl_logic;
        Mem_mem_R_EN    : in     vl_logic;
        MEM_WB_EN       : in     vl_logic;
        EXE_WB_EN       : in     vl_logic;
        branch          : in     vl_logic_vector(1 downto 0);
        hazard_Detected : out    vl_logic
    );
end hazard_detection_unit;
