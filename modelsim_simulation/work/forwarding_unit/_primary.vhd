library verilog;
use verilog.vl_types.all;
entity forwarding_unit is
    port(
        rst             : in     vl_logic;
        WB_EN           : in     vl_logic;
        MEM_WB_EN       : in     vl_logic;
        MEM_W_EN        : in     vl_logic;
        src1            : in     vl_logic_vector(4 downto 0);
        src2            : in     vl_logic_vector(4 downto 0);
        ST_Src          : in     vl_logic_vector(4 downto 0);
        WR_Dest         : in     vl_logic_vector(4 downto 0);
        MEM_Dest        : in     vl_logic_vector(4 downto 0);
        s1              : out    vl_logic_vector(1 downto 0);
        s2              : out    vl_logic_vector(1 downto 0);
        s3              : out    vl_logic_vector(1 downto 0)
    );
end forwarding_unit;
