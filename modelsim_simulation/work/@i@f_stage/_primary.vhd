library verilog;
use verilog.vl_types.all;
entity IF_stage is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        Br_taken        : in     vl_logic;
        hazard          : in     vl_logic;
        Br_offset       : in     vl_logic_vector(31 downto 0);
        PC              : out    vl_logic_vector(31 downto 0);
        Instruction     : out    vl_logic_vector(31 downto 0)
    );
end IF_stage;
