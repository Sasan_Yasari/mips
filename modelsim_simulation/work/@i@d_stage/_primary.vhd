library verilog;
use verilog.vl_types.all;
entity ID_stage is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        Instruction     : in     vl_logic_vector(31 downto 0);
        reg1            : in     vl_logic_vector(31 downto 0);
        reg2            : in     vl_logic_vector(31 downto 0);
        src1            : out    vl_logic_vector(4 downto 0);
        src2            : out    vl_logic_vector(4 downto 0);
        src1_f          : out    vl_logic_vector(4 downto 0);
        src2_f          : out    vl_logic_vector(4 downto 0);
        IF_flush        : out    vl_logic;
        Dest            : out    vl_logic_vector(4 downto 0);
        \Reg2\          : out    vl_logic_vector(31 downto 0);
        Val2            : out    vl_logic_vector(31 downto 0);
        Val1            : out    vl_logic_vector(31 downto 0);
        Br_taken        : out    vl_logic;
        EXE_CMD         : out    vl_logic_vector(3 downto 0);
        MEM_R_EN        : out    vl_logic;
        MEM_W_EN        : out    vl_logic;
        WB_EN           : out    vl_logic;
        is_imm          : out    vl_logic;
        st_or_bne       : out    vl_logic;
        br              : out    vl_logic_vector(1 downto 0)
    );
end ID_stage;
