library verilog;
use verilog.vl_types.all;
entity data_memory is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        MEM_W_EN        : in     vl_logic;
        MEM_R_EN        : in     vl_logic;
        ALU_result      : in     vl_logic_vector(31 downto 0);
        ST_value        : in     vl_logic_vector(31 downto 0);
        data            : out    vl_logic_vector(31 downto 0)
    );
end data_memory;
