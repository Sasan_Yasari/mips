library verilog;
use verilog.vl_types.all;
entity WB_stage is
    port(
        clk             : in     vl_logic;
        MEM_R_EN        : in     vl_logic;
        ALU_result      : in     vl_logic_vector(31 downto 0);
        Mem_read_value  : in     vl_logic_vector(31 downto 0);
        Write_value     : out    vl_logic_vector(31 downto 0)
    );
end WB_stage;
