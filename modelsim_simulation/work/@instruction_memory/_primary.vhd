library verilog;
use verilog.vl_types.all;
entity Instruction_memory is
    port(
        rst             : in     vl_logic;
        in_Ins          : in     vl_logic_vector(31 downto 0);
        out_Ins         : out    vl_logic_vector(31 downto 0)
    );
end Instruction_memory;
