library verilog;
use verilog.vl_types.all;
entity mux2 is
    port(
        sel             : in     vl_logic;
        in1_mux         : in     vl_logic_vector(4 downto 0);
        in2_mux         : in     vl_logic_vector(4 downto 0);
        out_mux         : out    vl_logic_vector(4 downto 0)
    );
end mux2;
