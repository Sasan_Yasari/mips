library verilog;
use verilog.vl_types.all;
entity EXE_stage is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        s1              : in     vl_logic_vector(1 downto 0);
        s2              : in     vl_logic_vector(1 downto 0);
        s3              : in     vl_logic_vector(1 downto 0);
        EXE_CMD         : in     vl_logic_vector(3 downto 0);
        val1            : in     vl_logic_vector(31 downto 0);
        val2            : in     vl_logic_vector(31 downto 0);
        reg2            : in     vl_logic_vector(31 downto 0);
        alu_result_mem  : in     vl_logic_vector(31 downto 0);
        result_wb       : in     vl_logic_vector(31 downto 0);
        ALU_result      : out    vl_logic_vector(31 downto 0);
        st_val          : out    vl_logic_vector(31 downto 0)
    );
end EXE_stage;
