module SRAM_Controller(
	input clk, 
	input rst, 
	input[31:0] SRAM_address, 
	input[31:0] SRAM_write_data, 
	input Sram_r_en, 
	input Sram_we_en, 
	output reg[63:0] SRAM_read_data, 
	output reg ready, 
	inout reg[15:0] SRAM_DATA, 
	output reg[17:0] SRAM_ADDRESS, 
	output reg SRAM_UB_N_O, 
	output reg SRAM_LB_N_O, 
	output reg SRAM_WE_N_O, 
	output reg SRAM_CE_N_O, 
	output reg SRAM_OE_N_O
	);
	
	reg[1:0] counter;

	always@(negedge clk, posedge rst) begin
		if(rst)
			counter = 0;
		else if(Sram_r_en || Sram_we_en)
			counter = counter + 2'b1;
		else
			counter = 0;
	end
	
	always@(*) begin
		SRAM_OE_N_O = 0;
		SRAM_UB_N_O = 0;
		SRAM_LB_N_O = 0;
		SRAM_CE_N_O = 0;
		SRAM_ADDRESS = (counter == 2'd0) ? {SRAM_address[17:2], 2'b00} :
							(counter == 2'd1) ? {SRAM_address[17:2], 2'b01} :
							(counter == 2'd2) ? {SRAM_address[17:2], 2'b10} :
							{SRAM_address[17:2], 2'b11};
		if(rst) begin
			SRAM_WE_N_O = 1;
			SRAM_read_data = 0;
			ready = 1;
			SRAM_DATA = 0;
			SRAM_ADDRESS = 0;
		end
		else begin
			if(Sram_r_en) begin
				SRAM_WE_N_O = 1;
				if(counter == 0)
					SRAM_read_data[15:0] = SRAM_DATA;
				else if(counter == 1)
					SRAM_read_data[31:16] = SRAM_DATA;
				else if(counter == 2'd2)
					SRAM_read_data[47:32] = SRAM_DATA;
				else if(counter == 2'd3)
					SRAM_read_data[63:48] = SRAM_DATA;
				else
					SRAM_DATA = 16'bz;
			end
			else if(Sram_we_en) begin
				if(counter == 0) begin
					SRAM_WE_N_O = 0;
					SRAM_DATA = SRAM_write_data[15:0];
				end
				else if(counter == 1)begin
					SRAM_WE_N_O = 0;
					SRAM_DATA = SRAM_write_data[31:16];
				end
				else begin
					SRAM_WE_N_O = 1;
					SRAM_DATA = 16'bz;
				end
			end
			else begin
				SRAM_WE_N_O = 1;
				SRAM_DATA = 16'bz;
			end
			if(counter == 2'b11)
				ready = 1;
			else if(Sram_r_en || Sram_we_en) 
				ready = 0;
			else
				ready = 1;
		end
	end
endmodule
