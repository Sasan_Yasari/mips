module IF_stage(
	input clk, 
	input rst, 
	input Br_taken, 
	input hazard, 
	input[31:0] Br_offset, 
	output[31:0] PC, 
	output[31:0] Instruction
	);
	
	wire[31:0] out_mux, out_PC, out_adder;
	mux m(Br_taken, 32'd4, (Br_offset<<2) - 32'd4, out_mux);
	adder ad(out_mux, out_PC, out_adder);
	PC p(clk, rst, hazard, out_adder, out_PC);
	Instruction_memory im(rst, out_PC, Instruction);
	assign PC = out_PC;
	
endmodule
