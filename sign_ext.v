module sign_ext(
	input rst, 
	input[15:0] in_sign, 
	output reg[31:0] out_sign
	);
	
	always@(*) begin
		if(rst)
			out_sign = 0;
		else
			out_sign <= $signed(in_sign);
	end
endmodule
