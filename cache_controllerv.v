module cache_controller(
	input clk, 
	input rst, 
	
	input [31:0] address, 
	input [31:0] wdata,
	input MEM_R_EN,
	input MEM_W_EN,
	output reg[31:0] rdata,
	output ready,
	
	output reg [31:0] sram_address,
	output reg [31:0] sram_wdata,
	output reg read,
	output reg write,
	input [63:0] sram_rdata,
	input sram_ready
	);
	
	reg [63:0] data0 [0:63];
	reg [8:0] tag0 [0:63];
	reg valid0 [0:63];
	
	reg [63:0] data1 [0:63];
	reg [8:0] tag1 [0:63];
	reg valid1 [0:63];
	
	reg lru [0:63];
	
	integer i;
	reg ready_;
	
	always@(*) begin
		if(rst) begin
			for(i = 0; i < 64; i = i + 1) begin
				data0[i] = 0;
				data1[i] = 0;
				tag0[i] = 0;
				tag1[i] = 0;
				valid0[i] = 0;
				valid0[i] = 0;
				lru[i] = 1;
			end
			read = 0;
			ready_ = 0;
			write = 0;
		end
		else begin
			//read
			
			if(MEM_R_EN) begin
				write = 0;
				if(address[2] == 0) begin
					if(tag0[address[8:3]] == address[17:9] && valid0[address[8:3]] == 1) begin
						rdata = data0[address[8:3]][31:0];
						ready_ = 1;
						read = 0;
					end
					else if(tag1[address[8:3]] == address[17:9] && valid1[address[8:3]] == 1) begin
						rdata = data1[address[8:3]][31:0];
						ready_ = 1;
						read = 0;
					end
					else begin
						read = 1;
						ready_ = 0;
						if(lru[address[8:3]] == 0) begin
							if(address[2] == 0) begin
								rdata = sram_rdata[31:0];
								data1[address[8:3]] = sram_rdata[31:0];
							end
							else begin
								rdata = sram_rdata[63:32];
								data1[address[8:3]] = sram_rdata[63:32];
							end
							valid1[address[8:3]] = 1;
							lru[address[8:3]] = 1;
						end
						else begin
							if(address[2] == 0) begin
								rdata = sram_rdata[31:0];
								data0[address[8:3]] = sram_rdata[31:0];
							end
							else begin
								rdata = sram_rdata[63:32];
								data0[address[8:3]] = sram_rdata[63:32];
							end
							valid0[address[8:3]] = 1;
							lru[address[8:3]] = 0;
						end
					end
				end
				else begin
					if(tag0[address[8:3]] == address[17:9] && valid0[address[8:3]] == 1) begin
						rdata = data0[address[8:3]][63:32];
						ready_ = 1;
						read = 0;
					end
					else if(tag1[address[8:3]] == address[17:9] && valid1[address[8:3]] == 1) begin
						rdata = data1[address[8:3]][63:32];
						ready_ = 1;
						read = 0;
					end
					else begin
						read = 1;
						ready_ = 0;
						if(lru[address[8:3]] == 0) begin
							if(address[2] == 0) begin
								rdata = sram_rdata[31:0];
								data1[address[8:3]] = sram_rdata[31:0];
							end
							else begin
								rdata = sram_rdata[63:32];
								data1[address[8:3]] = sram_rdata[63:32];
							end
							valid1[address[8:3]] = 1;
							lru[address[8:3]] = 1;
						end
						else begin
							if(address[2] == 0) begin
								rdata = sram_rdata[31:0];
								data0[address[8:3]] = sram_rdata[31:0];
							end
							else begin
								rdata = sram_rdata[63:32];
								data0[address[8:3]] = sram_rdata[63:32];
							end
							valid0[address[8:3]] = 1;
							lru[address[8:3]] = 0;
						end
					end
				end
			end
			else begin
				read = 0;
				write = 0;
			end			
			//write
			if(MEM_W_EN) begin
				read = 0;
				write = 1;
				sram_wdata = wdata;
				sram_address = address;
			end
			else begin
				read = 0;
				write = 0;
			end
		end
	end
	assign ready = sram_ready || ready_;
endmodule
