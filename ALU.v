module ALU(
	input rst, 
	input[31:0] in1, 
	input[31:0] in2, 
	input[3:0] cmd, 
	output reg[31:0] result
	);
	
	always@(*) begin
		if(rst)
			result = 0;
		else begin
			case(cmd)
				4'b0000: result = in1 + in2;
				4'b0010: result = in1 - in2;
				4'b0100: result = in1 & in2;
				4'b0101: result = in1 | in2;
				4'b0110: result = ~(in1 | in2);
				4'b0111: result = in1 ^ in2;
				4'b1000: result = in1 << in2;
				4'b1001: result = in1 >>> in2;
				4'b1010: result = in1 >> in2;
				4'b1100: result = in1 <<< in2;
				default: result = 32'b0;
			endcase
		end
	end
endmodule
