module MEM_stage_reg(
	input clk, 
	input rst, 
	input freeze, 
	input WB_en_in, 
	input MEM_R_EN_in, 
	input[31:0] PC_in, 
	input[31:0] ALU_result_in, 
	input[31:0] Mem_read_value_in, 
	input[4:0] Dest_in, 
	output reg WB_en, 
	output reg MEM_R_EN, 
	output reg[31:0] PC, 
	output reg[31:0] ALU_result, 
	output reg[31:0] MEM_read_value, 
	output reg[4:0] Dest
	);
	
	always@(negedge clk, posedge rst) begin
		if(rst) begin
			WB_en = 0;
			MEM_R_EN = 0;
			ALU_result = 0;
			MEM_read_value = 0;
			Dest = 0;
			PC = 0;
		end
		else if(freeze) begin
			WB_en = WB_en;
			MEM_R_EN = MEM_R_EN;
			ALU_result = ALU_result;
			MEM_read_value = MEM_read_value;
			Dest = Dest;
			PC = PC;
		end
		else begin
			WB_en = WB_en_in;
			MEM_R_EN = MEM_R_EN_in;
			ALU_result = ALU_result_in;
			MEM_read_value = Mem_read_value_in;
			Dest = Dest_in;
			PC = PC_in;
		end
	end
	
endmodule
