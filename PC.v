module PC(
	input clk, 
	input rst, 
	input freeze, 
	input[31:0] in_PC, 
	output reg[31:0] out_PC
	);
	
	always@(negedge clk) begin
		if(rst)
			out_PC = 0;
		else if(freeze == 1)
			out_PC = out_PC;
		else
			out_PC = in_PC;
	end
endmodule
