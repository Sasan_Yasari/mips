module forwarding_unit(
	input rst,
	input WB_EN, 
	input MEM_WB_EN, 
	input MEM_W_EN, 
	input[4:0] src1, 
	input[4:0] src2, 
	input[4:0] ST_Src, 
	input[4:0] WR_Dest, 
	input[4:0] MEM_Dest, 
	output reg[1:0] s1, 
	output reg[1:0] s2, 
	output reg[1:0] s3
	);
	
	always@(*) begin
		if(rst) begin
			s1 = 0;
			s2 = 0;
			s3 = 0;
		end
		else begin
			if(MEM_WB_EN && src1 == MEM_Dest && MEM_Dest != 4'b0)
				s1 = 2'd1;
			else if(WB_EN && src1 == WR_Dest && WR_Dest != 4'b0)
				s1 = 2'd2;
			else
				s1 = 2'd0;
			
			if(MEM_WB_EN && src2 == MEM_Dest && MEM_Dest != 4'b0)
				s2 = 2'd1;
			else if(WB_EN && src2 == WR_Dest && WR_Dest != 4'b0)
				s2 = 2'd2;
			else
				s2 = 2'd0;
			
			if(MEM_W_EN && ST_Src == MEM_Dest && MEM_WB_EN && MEM_Dest != 4'b0)
				s3 = 2'd1;
			else if(MEM_W_EN && ST_Src == WR_Dest && WB_EN && WR_Dest != 4'b0)
				s3 = 2'd2;
			else
				s3 = 2'd0;
		end
	end
endmodule
