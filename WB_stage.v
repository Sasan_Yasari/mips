module WB_stage(
	input clk, 
	input MEM_R_EN, 
	input[31:0] ALU_result, 
	input[31:0] Mem_read_value, 
	output[31:0] Write_value
	);
	
	mux m(MEM_R_EN, ALU_result, Mem_read_value, Write_value);
endmodule
